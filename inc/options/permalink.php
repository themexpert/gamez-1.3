<?php

function permalink_framework_options( $options ) {

    $options[]    = array(
        'name'      => 'tx_gamez_permalink',
        'title'     => esc_html__('Permalink', 'gamez'),
        'icon'      => 'fa fa-link',
        'fields'    => array(

            /**
             * Custom Permalink
             */

            array(
                'id'           => 'tx_gamez_permalink_enable',
                'type'         => 'switcher',
                'title'        => __('Custom Permalink', 'gamez'),
                'desc'         => __('Enable custom permalink for Custom Post Type and Taxonomy.', 'gamez'),
            ),

            /**
             * Post Type Slug
             */

            array(
                'type'    => 'heading',
                'content' => __('Post Type', 'gamez'),
                'dependency'   => array( 'tx_gamez_permalink_enable', '==', 'true' ),
            ),

            array(
                'id'      => 'tx_gamez_permalink_reviews',
                'type'    => 'text',
                'title'   => __('Game Reviews Slug', 'gamez'),
                'desc'    => __('Type your desired slug for Game Reviews Post Type.', 'gamez'),
                'dependency'   => array( 'tx_gamez_permalink_enable', '==', 'true' ),
            ),

            array(
                'id'      => 'tx_gamez_permalink_galleries',
                'type'    => 'text',
                'title'   => __('Video Galleries Slug', 'gamez'),
                'desc'    => __('Type your desired slug for Video Galleries Post Type.', 'gamez'),
                'dependency'   => array( 'tx_gamez_permalink_enable', '==', 'true' ),
            ),

            array(
                'type'    => 'heading',
                'content' => __('Taxonomy', 'gamez'),
                'dependency'   => array( 'tx_gamez_permalink_enable', '==', 'true' ),
            ),

            array(
                'id'      => 'tx_gamez_permalink_review_genre',
                'type'    => 'text',
                'title'   => __('Game Genre Slug', 'gamez'),
                'desc'    => __('Type your desired slug for Game Genre Taxonomy.', 'gamez'),
                'dependency'   => array( 'tx_gamez_permalink_enable', '==', 'true' ),
            ),

            array(
                'id'      => 'tx_gamez_permalink_review_category',
                'type'    => 'text',
                'title'   => __('Game Review Category Slug', 'gamez'),
                'desc'    => __('Type your desired slug for Game Review Category Taxonomy.', 'gamez'),
                'dependency'   => array( 'tx_gamez_permalink_enable', '==', 'true' ),
            ),

            array(
                'id'      => 'tx_gamez_permalink_gallery_category',
                'type'    => 'text',
                'title'   => __('Gallery Category Slug', 'gamez'),
                'desc'    => __('Type your desired slug for Gallery Category Taxonomy.', 'gamez'),
                'dependency'   => array( 'tx_gamez_permalink_enable', '==', 'true' ),
            ),



        )
    );

    return $options;

}
add_filter( 'cs_framework_options', 'permalink_framework_options' );
