<?php
/**
 * actions files are included here
 */

require get_template_directory().'/inc/actions/woocommerce-support.php';
require get_template_directory().'/inc/actions/shop-single.php';
require get_template_directory().'/inc/actions/single-featured-image.php';
require get_template_directory().'/inc/actions/cart-count.php';
require get_template_directory().'/inc/actions/gamez-actions.php';
