<?php

function gamez_change_post_types_slug($args, $post_type) {

    $permalink_enable = cs_get_option('tx_gamez_permalink_enable');


    if (('game_review' === $post_type ) && $permalink_enable && cs_get_option('tx_gamez_permalink_reviews')) {
        $args['rewrite']['slug'] = cs_get_option('tx_gamez_permalink_reviews');
    }


    if (('video_gallery' === $post_type ) && $permalink_enable && cs_get_option('tx_gamez_permalink_galleries')) {
        $args['rewrite']['slug'] = cs_get_option('tx_gamez_permalink_galleries');
    }

    return $args;
}

add_filter('register_post_type_args', 'gamez_change_post_types_slug', 10, 2);



function gamez_change_taxonomies_slug( $args, $taxonomy ) {

    $permalink_enable = cs_get_option('tx_gamez_permalink_enable');


    if (('game_genre' === $taxonomy ) && $permalink_enable && cs_get_option('tx_gamez_permalink_review_genre')) {
        $args['rewrite']['slug'] = cs_get_option('tx_gamez_permalink_review_genre');
    }

    if (('game_category' === $taxonomy ) && $permalink_enable && cs_get_option('tx_gamez_permalink_review_category')) {
        $args['rewrite']['slug'] = cs_get_option('tx_gamez_permalink_review_category');
    }

    if (('video_category' === $taxonomy ) && $permalink_enable && cs_get_option('tx_gamez_permalink_gallery_category')) {
        $args['rewrite']['slug'] = cs_get_option('tx_gamez_permalink_gallery_category');
    }


    return $args;
}
add_filter( 'register_taxonomy_args', 'gamez_change_taxonomies_slug', 10, 2 );

